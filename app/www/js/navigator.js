
var navigatorHelper = {
    pages: [],
    currentPageIndex: -1,

    initialize: function() {
      this.pages = [
        $('#page1'),
        $('#page2'),
        $('#page3'),
      ];
      this.currentPageIndex = 1;
    },

    toggleFromSource: function(articleId) {
      layoutHelper.setPage3Content(articleId);
      this.navigateToPage(3);
    },

    navigateLeft: function() {
      this.slidePageTo('left');
    },

    navigateRight: function() {
      this.slidePageTo('right');
    },

    navigateToPage: function(pageNumber) {
      layoutHelper.closeMenu();
      this.scrollPagetoTop();
      if (pageNumber === 1) {
        layoutHelper.closeMenu();
      }

      this.pages.forEach(function($el, idx) {
        if (pageNumber === idx + 1) {
          $el.removeClass('hide');
        } else {
          $el.addClass('hide');
        }
      });
    },

    slidePageTo: function(to) {
      //close the menu if open;
      layoutHelper.closeMenu();
      
      if (to === "left" && this.currentPageIndex > 0) {
        newCurrentPageIndex = this.currentPageIndex-1;
      } else if (to === "right" && this.currentPageIndex < this.pages.length-1 ) {
        newCurrentPageIndex = this.currentPageIndex+1;
      } else {
        return;
      }

      var currentPage = this.pages[this.currentPageIndex];
      var nextPage = this.pages[newCurrentPageIndex];
      
      // Position the page at the starting position of the animation
      //nextPage.attr("class", "page center");      
      // Position the new page and the current page at the ending position of their animation with a transition class indicating the duration of the animation
      nextPage.attr("class", "page transition center");

      currentPage.attr("class", "page transition " + (to == 'left' ? 'right' : 'left'));
      this.currentPageIndex = newCurrentPageIndex;
    },

    scrollPagetoTop: function(element){
      $('#bottom-nav1').stop(true, true).fadeOut();
      $('#bottom-nav2').stop(true, true).fadeOut();
      $('#bottom-nav3').stop(true, true).fadeOut();
      $('html,body').animate({scrollTop: 0}, 0);
    },
};


