var dataHelper = {

    relations: [],
    page1SearchEngine: {},
    page2SearchEngine: {},
    page3SearchEngine: {},

    // Constructor
    initialize: function() {

        //parse the csv for loading content on right page/3
        this.parseRelationTable();

        this.page1SearchEngine = new SearchEngine($("#page1-content"), $("#page1-search-field"), $("#page1-search-reset"));
        this.page1SearchEngine.initialize();

        this.page2SearchEngine = new SearchEngine($("#page2-content"), $("#page2-search-field"), $("#page2-search-reset"));
        this.page2SearchEngine.initialize();

        this.page3SearchEngine = new SearchEngine($("#page3-content"), $("#page3-search-field"), $("#page3-search-reset"));
        this.page3SearchEngine.initialize();
    },

    getNewCPCArticle: function(articleId) {
        var content = $("#page2-content").find('#' + articleId);
        return content.html();
    },

    getOldCPCArticlesIdsFromNew: function(articleId) {
        var oldIds = relations[articleId -1][1];
        if (oldIds.length == 0) {
            return [];
        };
        var oldIdsArray = oldIds.split(",");
        return oldIdsArray;
    },

    getOldCPCArticlesFromNew: function(articleId) {
        //check which articles correspond in hastable
        //and load html from first page
        var oldIdsArray = this.getOldCPCArticlesIdsFromNew(articleId);
        if (oldIdsArray == null || oldIdsArray.length == 0) {
            return "";
        }

        var oldArticlesHtml = "";
        oldIdsArray.forEach(function(entry) {
            entry = entry.replace(" ", ""); 
            var contentOfEach = $("#page1-content").find('#' + entry);
            oldArticlesHtml += contentOfEach.html();
            oldArticlesHtml += "<hr>\n"
        });
        return oldArticlesHtml;
    },

    resetSearch: function(){
        this.page1SearchEngine.resetSearch();
        this.page2SearchEngine.resetSearch();
        this.page3SearchEngine.resetSearch();
    },


    parseRelationTable: function() {
        $.get("static/correspondencias.csv", function(text)
        {
            //parse;
            Papa.parse(text, {
                complete: function(results) {
                    relations = results.data;
                    layoutHelper.processRelations(relations);
                }
            });
        });
    },
};


/*
    varialbles are the element JQuery objects
*/
function SearchEngine(containerElement, searchFieldElement, resetSearchElement) {

    var container = containerElement,
        searchField = searchFieldElement,
        reset = resetSearchElement,
        currentSearchElementPosition = -1,
        currentSearchQuery = "",
        currentSearchCount = -1;

    var $page               = containerElement.closest('.page');
    var $WSearch            = $page.find('.w-search');
    var $hiddenSearch       = $page.find('.hidden-search');
    var $searchCount        = $page.find('.search-count');
    var $currentSearchCount = $page.find('.current-search-count');


    this.initialize = function() {
        var that = this;
        searchField.keypress(function (e) {
            if (e.which == 13) {
                that.searchDown();
                return false;
            }
        });
    };

    this.searchUp = function() {
        this.doSearch(searchField.val(), -1);
    };

    this.searchDown = function() {
        this.doSearch(searchField.val(), 1);
    };

    this.doSearch = function(query, increment) {
        //reset the previous search if the terms are different or non hexistent
        if (query.length == 0 || this.currentSearchQuery != query) {
            this.resetSearchElements();
        }

        //if the query is empty return after the reset
        if (query.length == 0) return;

        // add the reset icon
        reset.removeClass("hide");

        // Show the search navigation bar for mobile 
        $hiddenSearch.addClass('show');
        $('.w-wrapper').addClass('expand');

        // Show the search buttons 
        $page.find('.search-btn').removeClass('hide');

        // Show the search label
        $page.find('.search-count-label').removeClass('hide');

        //highlight
        this.currentSearchQuery = query;
        if (currentSearchCount < 0) {
            container.highlight(query);
            currentSearchCount = $page.find('.highlight').length;

            // Update search count
            $searchCount.text(currentSearchCount);
        }

        //set the search element to the first if no element is currently selected
        if (this.currentSearchElementPosition == -1) {
            this.currentSearchElementPosition = 0;
        }

        //set the search element to the next/previous element after the current 
        else {
            this.currentSearchElementPosition += increment;
            if (this.currentSearchElementPosition < 0) {
                this.currentSearchElementPosition = currentSearchCount - 1;
            } else if (this.currentSearchElementPosition >= currentSearchCount) {
                this.currentSearchElementPosition = 0;
            }
        }

        // Update current search count
        if (currentSearchCount == 0) {
            $currentSearchCount.text(0);
        } else {
            $currentSearchCount.text(this.currentSearchElementPosition + 1);
        }
        

        container.find('.highlightstrong').removeClass("highlightstrong");
        var element = $page.find( ".highlight:eq("+this.currentSearchElementPosition+")" );
        element.addClass("highlightstrong");
        
        // scroll to the element using velocity
        // container.removeClass("momentum-scroll");
        this.scrollGo(element);
        // container.addClass("momentum-scroll");
    };

    this.scrollGo = function(element) {
       element.velocity("scroll", { 
          duration: 600,
          delay: 0,
          offset: -300,
          mobileHA: false
        });
    };

    this.resetSearch = function() {
        reset.addClass("hide");
        searchField.val("");
        this.resetSearchElements();
    };

    this.resetSearchElements = function() {
        this.currentSearchElementPosition = -1;
        this.currentSearchQuery = "";
        currentSearchCount = -1;
        container.unhighlight();
        //$('.highlight').removeClass("highlight");
        container.find('.highlightstrong').removeClass("highlightstrong");
        
        reset.addClass("hide");
        $hiddenSearch.removeClass("show");
        $('.w-wrapper').removeClass('expand');
        $page.find('.search-btn').addClass('hide');
        $page.find('.search-count-label').addClass('hide');
    };


}
