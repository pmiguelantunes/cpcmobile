var layoutHelper = {

  // Application Constructor
  initialize: function() {
    this.showSpinner();
    this.loadPage1();
    this.loadPage2();
    this.loadPage3();
  },

  onPage1Ready: function() {
    //this is called on document ready of page 1
  },

  onPage2Ready: function() {
    //this is called on document ready of page 2
    this.processLinks();

    //hack to Disable the ios next and previous arrows
    eventName = "click";
    /*eventName = "";
    if ( $.mobile ) {
      eventName = "tap";
    } else {
      eventName = "click";
    } */

    $("input[type='search']").on(eventName, function() {
        //console.log("Disabling others");
        $("input[type='search']").not(this).attr("readonly", "readonly");
        $("input[type='search']").not(this).attr("disabled", "disabled");
    });

    $("input[type='search']").on('blur', function() {
        //console.log("Enabling others");
        $("input[type='search']").removeAttr("readonly");
        $("input[type='search']").removeAttr("disabled");
    });

    this.hideSpinner();
  },

  onPage3Ready: function() {
    //this is called on document ready of page 3
  },

  /////
  // Internal methods (Called from inside)
  /////

  
  showSpinner: function() {
    //console.log("Showing spinner");
    $.mobile.loading( "show", {
      text: "",
      textVisible: false,
      theme: "b",
      html: '<div><i class="fa fa-spinner fa-spin fa-fontsize-big red"></i></div>'
    });
    $(".ui-loader").show();
  },

  hideSpinner: function() {
    //console.log("Hiding spinner");
    $(".ui-loader").hide();
  },

  loadPage1: function() {
    $.get('page1.html', function(template) {
      /*
      var rendered = Handlebars.compile(template);
      $('#page1').html(rendered);
      $('#page1-content').load( "static/cpc_1973.html", function() {});
      */
      $.get("static/cpc_1973.html", function(content) {
        var rendered = Handlebars.compile(template);
        $('#page1').html(rendered); 
        $("#page1-content").html(content);
      });
    });
  },

  loadPage2: function() {
    //load the template page and then the static content into it
    $.get('page2.html', function(template) {
      $.get("static/cpc_2015.html", function(content) {
        var rendered = Handlebars.compile(template);
        $('#page2').html(rendered); 
        $("#page2-content").html(content);

        //load the about content
        //container is inside page 2 so we need to call it here
        layoutHelper.loadAbout();

        //parse the relations csv and 
        //filter the relations links. 
        //(hide those that don't have relation)
        dataHelper.initialize();
      });
    });
  },

  loadPage3: function() {
    $.get('page3.html', function(template) {
      var rendered = Handlebars.compile(template);
      $('#page3').html(rendered);
    });
  },

  loadAbout: function() {
    $.get('about.html', function(template) {
      var rendered = Handlebars.compile(template);
      $('#page2-about').html(rendered);
    });
  },

  about: function() {
    $('#page2-about').removeClass("hide");
    $('#page2-wrapper').addClass("hide");
  },

  closeAbout: function() {
    $('#page2-wrapper').removeClass("hide");
    $('#page2-about').addClass("hide");
  },

  setPage3Content: function(articleId) {
      var newArticleHtml = dataHelper.getNewCPCArticle(articleId);
      $('#new-cpc-article-content').html(newArticleHtml);

      var oldArticlesHtml = dataHelper.getOldCPCArticlesFromNew(articleId);
      $('#old-cpc-articles-content').html(oldArticlesHtml);

      layoutHelper.setPage3ShareButtons(articleId);

      //reset the search just in case
      dataHelper.page3SearchEngine.resetSearch();
  },

  setPage3ShareButtons: function(articleId){

    eventName = "click";
    /*eventName = "";
    if ( $.mobile ) {
      eventName = "tap";
    } else {
      eventName = "click";
    } */

    var title = 'CPC Comparador';
    var link = "https://www.facebook.com/pages/CPCcomparado/111469422520029";
    var caption = "shared from cpc";
    var oldArticles = dataHelper.getOldCPCArticlesIdsFromNew(articleId);
    var description = "Acabei de usar o CPC Comparador para comparar o artigo "+articleId+"º do CPC 2015 com "

    //description string construction
    if(oldArticles.length>1)
    {
      description=description+"o(s) artigo(s) "+oldArticles[0];

      for (var i=1; i < oldArticles.length-1; i++) {
        console.log(i);
        description=description+"º, "+oldArticles[i];
      }
      description=description+"º e "+oldArticles[oldArticles.length-1]+"º";
    }
    else{ 
      description=description+"o artigo "+oldArticles[0]+"º"
    }

    description=description+" do CPC 1973";


    //facebook button
    $('#share-social').bind(eventName, function(e){
      e.preventDefault();
      window.plugins.socialsharing.share(description, title, null, link);
    });
  },

  openMenu: function() {
      $('.toggle-nav ul, .toggle-nav i.fa-times').show();
      $('.toggle-nav i.fa-bars').hide();
      $('.toggle-nav').addClass('open');
  }, 
  closeMenu: function() {
      $('.toggle-nav ul, .toggle-nav i.fa-times').hide();
      $('.toggle-nav i.fa-bars').show();
      $('.toggle-nav').removeClass('open'); 
  }, 

  processRelations: function (relations) {
    for (index = 1; index <= relations.length; index++) {
      //remove the href and change the style to muted
      var element = $("#link-" + index);
      if (relations[index-1][1] == null || relations[index-1][1].length <= 0) {
        $(element).removeClass("red");
        $(element).addClass("muted");
      
      } else {

        eventName = "click";
        /*eventName = "";
        if ( $.mobile ) {
          eventName = "tap";
        } else {
          eventName = "click";
        } */
        $(element).on(eventName,function(event){
          javascript:navigatorHelper.toggleFromSource($(event.target).attr("data-id"));
        });
      }
    }
  },


  processLinks: function () {
    eventName = "click";
    /*eventName = "";
    if ( $.mobile ) {
      eventName = "tap";
    } else {
      eventName = "click";
    } */

    //menu 
    this.setupMenu(eventName);

    //search up and down
    this.setupSearchButtons(eventName);
  },


  setupMenu: function(eventName) {

    $('.toggle-nav i.fa-bars').on(eventName, function() {
      layoutHelper.openMenu();
    });

    $('.toggle-nav i.fa-times').on(eventName, function() {
      layoutHelper.closeMenu();
    });

    $('.toggle-nav ul li a').on(eventName, function() {
      $(this).find('span').toggleClass('hide');
    });



    //setup scroll top for page1
    $('#page1-content').scroll(function() {
      if ($('#page1-content').scrollTop()) {
        $('#bottom-nav1:hidden').stop(true, true).fadeIn();
      } else {
        $('#bottom-nav1').stop(true, true).fadeOut();
      }
    });
    $('#bottom-nav1').on(eventName, function() {
      navigatorHelper.scrollPagetoTop($('#page1-content'));
    });


    //setup scroll top for page2
    $('#page2-content').scroll(function() {
      alert("scrolling");
      if ($('#page2-content').scrollTop()) {
        $('#bottom-nav2:hidden').stop(true, true).fadeIn();
      } else {
        $('#bottom-nav2').stop(true, true).fadeOut();
      }
    });
    $('#bottom-nav2').on(eventName, function() {
      navigatorHelper.scrollPagetoTop($('#page2-content'));
    });

    //setup scroll top for page3
    $('#page3-content').scroll(function() {
      if ($('#page3-content').scrollTop()) {
        $('#bottom-nav3:hidden').stop(true, true).fadeIn();
      } else {
        $('#bottom-nav3').stop(true, true).fadeOut();
      }
    });
    $('#bottom-nav3').on(eventName, function() {
      navigatorHelper.scrollPagetoTop($('#page3-content'));
    });
  },

  setupSearchButtons: function(eventName) {
    //page1
    $('.page1-navigate-right').on(eventName, function() {
      navigatorHelper.navigateToPage(2);
    });
    $('.page1-search-up').on(eventName, function() {
      dataHelper.page1SearchEngine.searchUp();
    });
    $('.page1-search-down').on(eventName, function() {
      dataHelper.page1SearchEngine.searchDown();
    });
    $('#page1-search-reset').on(eventName, function() {
      dataHelper.page1SearchEngine.resetSearch();
    });

    //page2
    $('.page2-search-up').on(eventName, function() {
      dataHelper.page2SearchEngine.searchUp();
    });
    $('.page2-search-down').on(eventName, function() {
      dataHelper.page2SearchEngine.searchDown();
    });
    $('#page2-search-reset').on(eventName, function() {
      dataHelper.page2SearchEngine.resetSearch();
    });


    //page3
    $('.page3-navigate-left').on(eventName, function() {
      navigatorHelper.navigateToPage(2);
    });
    $('.page3-search-up').on(eventName, function() {
      dataHelper.page3SearchEngine.searchUp();
    });
    $('.page3-search-down').on(eventName, function() {
      dataHelper.page3SearchEngine.searchDown();
    });
    $('#page3-search-reset').on(eventName, function() {
      dataHelper.page3SearchEngine.resetSearch();
    });

  }

};
