Build and Deploy steps:

Android

1) Build for release. Zip aligns and signs with the release key 
>> Cordova build android --release


2) Run for final validation.
>> Cordova run android --release

3) Release
>> go to: https://play.google.com/apps/publish
