date = "2015"

f = File.open("static/cpc_" + date + ".html", "w")

is_first = true
is_open = false

File.open("static/cpc_" + date + "_orig.htm", "r").each_line do |line|

  # Encapsulate articles into divs
  data = line
  line.gsub(/<p>Art\. [0-9]{0,1}\.{0,1}[0-9]{1,3}\.{0,1}[a-z]{0,1}-{0,1}[A-Z]{0,1}/) do |article|
    if article.length > 0
      #puts "Found article: #{article}"
      #puts "Found article data: #{data}"
      f.write("</div>\n") unless is_first || !is_open
      is_first = false

      article2 = article.downcase
      if (article2.end_with? ".")
        article2 = article2.chop
      end
      #remove starting trash
      article2.gsub! '<p>art. ', ''
      #remove . between thousands
      article2.gsub! '1.', '1'

      if (date == "2015")
        f.write('<a href="#"><i id="link-' + article2.downcase + '" data-id="' + article2.downcase + '" class="fa fa-exchange fa-fontsize-big red link-anchor"></i></a>' + "\n") 
      end
      f.write("<div class='article' id='#{article2.downcase}'>\n")
      is_open = true
    end
  end

#1102.a

#615"
#/[IVX]* -/
#  "I -"

#618 - Paragrafos
#
#  "<li>5"
#544
#  a) 


#475-O§3



  data.gsub(/<p style="text-align: center;">/) do |processed_titles|
    if processed_titles.length > 0
      f.write("</div>\n") if is_open
      is_open = false
    end
  end

  # Encapsulate articles into divs
  data.gsub(/<p>CAP&Iacute;TULO|<p>PARTE|<p>LIVRO|<p>T&Iacute;TULO|<p>Se&ccedil;&atilde;o/) do |titles|
  # line.gsub(/<p>CAPÍTULO|<p>PARTE|<p>LIVRO|<p>TÍTULO|<p>Seção/) do |titles|
    if titles.length > 0
      puts "Found titles: #{titles}"
      puts "Found titles data: #{data}"

      if (titles == "<p>CAP&Iacute;TULO")
        class_part = "capitulo";
      elsif (titles == "<p>PARTE")
        class_part = "parte";
      elsif (titles == "<p>LIVRO")
        class_part = "livro";
      elsif (titles == "<p>T&Iacute;TULO")
        class_part = "titulo";
      elsif (titles == "<p>Se&ccedil;&atilde;o")
        class_part = "secao";
      end
      
      data = data.gsub '<p>', '<p class="' + class_part + '" style="text-align: center;">'

      f.write("</div>\n") if is_open
      is_open = false

      #f.write(new_sentence)
    end
  end



  data.gsub! 'true. ', ''
  f.write(data)
  
end